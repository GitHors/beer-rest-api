// External modules
const isJSON = require('is-valid-json');

// Modules
const url = require('url');

// Controller for beer data manipulation
const beerController = require('../controllers/beer');

// Constants for setting header of response
const headerContentTypeName = 'Content-Type';
const headerContentTypeTextValue = 'text/plain';
const headerContentTypeJSONValue = 'application/json';

// Messages for error handling
const errorAccessTokenMessage = 'Access-Token error';
const errorInvalidJSONFormat = 'Invalid JSON Format';
const errorInvalidJSONStructure = 'Invalid JSON Structure';
const errorInvalidID = 'There is no content with such ID';
const errorUnsupportedMethod = 'Unsupported method';
const errorRoutingMessage = 'There is no such resourse';

/*
  Function for checking access token from request (from header)
*/
function checkAccessToken(request) {
  return request.headers['access-token'] === 'some-token';
}

/*
  Function for sending JSON object as a response.
*/
function jsonResponse(response, content, statusCode) {
  if (statusCode) {
    response.statusCode = statusCode;
  }
  response.setHeader(headerContentTypeName, headerContentTypeJSONValue);
  response.end(content);
}


/*
  Function for setting TEXT message to a response
*/
function messageResponse(response, message, statusCode) {
  response.statusCode = statusCode;
  response.setHeader(headerContentTypeName, headerContentTypeTextValue);
  if (message) {
    response.end(message);
  }
}

/*
  Handling GET method.
*/
function handleGETQuery(request, response) {
  let result = JSON.stringify(beerController.getAll());
  jsonResponse(response, result);
}

/*
  Handling GET method with parameters.
*/
function handleGETByParamQuery(param, request, response) {
  beerController.get(param)
    .then((beer) => {
      jsonResponse(response, JSON.stringify(beer));
    })
    .catch(() => {
      // If there is no such data
      messageResponse(response, errorInvalidID, 404);
    });
}

/*
  Handling DELETE method. Only for client with valid Access-Token
*/
function handleDELETEQuery(param, request, response) {
  if (checkAccessToken(request)){
    beerController.remove(param)
      .then(() => {
        messageResponse(response, 'deleted', 201);
      })
      .catch(() => {
        // If there is no such data
        messageResponse(response, errorInvalidID, 204);
      });
  }  else {
    messageResponse(response, errorAccessTokenMessage, 401);
  }
}

/*
  Handling POST method. Only for client with valid Access-Token.
*/
function handlePOSTQuery(request, response) {
  let body = '';
  request.on('data', part => body += part);

  request.on('end', () => {
    if (checkAccessToken(request)){
      if (isJSON(body) && beerController.isBeerValid(body)) {
        let beer = JSON.parse(body);

        beerController.add(beer)
          .then(() => {
            // Created status code
            jsonResponse(response, JSON.stringify(beer), 201);
          })
          .catch(() => {
            // If there is no such data
            messageResponse(response, errorInvalidID, 400);
          });
      } else {
        // If we have some wrong JSON structure
        messageResponse(response, errorInvalidJSONStructure, 400);
      }
    } else {
      messageResponse(response, errorAccessTokenMessage, 401);
    }
  });
}

/*
  Handling PUT method. Only for client with valid Access-Token.
*/
function handlePUTQuery(param, request, response) {
  let body = '';
  request.on('data', part => body += part);

  request.on('end', function() {
    if (checkAccessToken(request)){
      if (isJSON(body) && beerController.isBeerValid(body)) {
        let beer = JSON.parse(body);
        beer.id = param;

        beerController.update(beer)
          .then(() => {
            // Accepted status code
            jsonResponse(response, JSON.stringify(beer), 202);
          })
          .catch(() => {
            // If there is no such data
            messageResponse(response, errorInvalidID, 400);
          });
      } else {
        // If we have some wrong JSON structure
        messageResponse(response, errorInvalidJSONStructure, 400);
      }
    } else {
      // 401 Unauthorized error
      messageResponse(response, errorAccessTokenMessage, 401);
    }
  });
}

/*
  Function for getting parameter from URL. (sorts/123 => 123)
*/
function parseQuery(queryUrl) {
  return queryUrl.match(/(\/sorts(\/){0,1})(([\w0-9]){1,}){0,1}/)[3];
}

/*
  Function for handling user requests.
*/
function queryTreatment(req, res) {
  let method = req.method;
  let queryUrl = req.url;
  let param = parseQuery(queryUrl);


  if (queryUrl.indexOf('/sorts') === 0) {
    // Handling query without params
    if (param === undefined) {
      switch (method) {
        case 'GET':
          handleGETQuery(req, res);
          break;
        case 'POST':
          handlePOSTQuery(req, res);
          break;
        default:
          messageResponse(res, errorUnsupportedMethod, 400);
          break;
      }
    } else {
      switch (method) {
        case 'DELETE':
          handleDELETEQuery(param, req, res);
          break;
        case 'GET':
          handleGETByParamQuery(param, req, res);
          break;
        case 'PUT':
          handlePUTQuery(param, req, res);
          break;
        default:
          messageResponse(res, errorUnsupportedMethod, 400);
          break;
      }
    }
  } else {
    messageResponse(res, errorRoutingMessage, 404);
  }
}

module.exports = {
  queryTreatment: queryTreatment
};
