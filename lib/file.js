// Modules
const fs = require('fs');

module.exports = {
  readFile: (file, callback) => {
    fs.readFile(file, 'utf-8', (err, data) => {
      if (err) {
        callback(err);
      }
      callback (null, data);
    });
  },
  saveFile: (file, data, callback) => {
    fs.writeFile(file, data, (err) => {
      if (err) {
        callback(err);
      }
      if (callback) {
        callback();
      }
    });
  }
}
