/*jshint esversion: 6 */

// Modules
const uniqid = require('uniqid');
// Local Modules
const file = require('../lib/file.js');
// Data storage
const beerList = require('../data/beer.json');

// Constants
const DATA_FILE_NAME = './data/beer.json';

/*
  Function for getting full list of beer sorts with short description.
*/
function getShortBeerDescriptionList(beerList) {
  let list = [];
  let size = beerList.length;
  for (let i = 0; i < size; i++) {
    let shortBeerInfo = {};
    shortBeerInfo.id = beerList[i].id;
    shortBeerInfo.name = beerList[i].name;
    shortBeerInfo.preview = beerList[i].preview;
    list.push(shortBeerInfo);
  }
  return list;
}


module.exports = {

  add: beer => {
    return new Promise((resolve, reject) => {
      beer.id = uniqid();
      beerList.push(beer);
      file.saveFile(DATA_FILE_NAME, JSON.stringify(beerList), resolve);
    });
  },

  getAll: () => {
    return getShortBeerDescriptionList(beerList);
  },

  remove: id => {
    return new Promise((resolve, reject) => {
      let size = beerList.length;
      for (let i = 0; i < size; i++) {
        if (beerList[i].id === id) {
          beerList.splice(i, 1);
          file.saveFile(DATA_FILE_NAME, JSON.stringify(beerList), resolve);
          return;
        }
      }
      // If there is no beer with such ID
      reject();
    });
  },

  get: id => {
    return new Promise((resolve, reject) => {
      let size = beerList.length;
      for (let i = 0; i < size; i++) {
        if (beerList[i].id === id) {
          resolve(beerList[i]);
          return;
        }
      }
      // If there is no beer with such ID
      reject();
    });
  },

  update: beer => {
    return new Promise((resolve, reject) => {
      let size = beerList.length;
      for (let i = 0; i < size; i++) {
        if (beerList[i].id === beer.id) {
          beerList[i] = beer;
          file.saveFile(DATA_FILE_NAME, JSON.stringify(beerList), resolve);
          return;
        }
      }
      // If there is no beer with such ID
      reject();
    });
  },

  isBeerValid: beer => {
    beer = JSON.parse(beer);
    let correctBeer = {
      name: beer.name,
      preview: beer.preview,
      abv: beer.abv,
      description: beer.description
    };
    for (let field in beer) {
      if (!correctBeer[field]) {
        return false;
      }
    }
    return true;
  }

};
