// Modules
const http = require('http');

// Rest treatment module
const rest = require('./services/rest');


const server = http.createServer((request, response) => {
  rest.queryTreatment(request, response);
});

server.listen(8080, () => {
  console.log('Server at port 8080');
});
